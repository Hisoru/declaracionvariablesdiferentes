
public class DeclaracionVariablesDiferentesApp {

	public static void main(String[] args) {
		
		// Declaraci�n de tres variables diferentes
		int N = 1;
		double A = 2.34;
		char C = 'e';
		
		// Muestra por consola de variables
		System.out.println("Variable N = " + N);
		System.out.println("Variable A = " + A);
		System.out.println("Variable C = " + C);
		
		// Muestra por consola de operaciones
		System.out.println(N + " + " + A + " = " + (N + A));
		System.out.println(A + " - " + N + " = " + (A - N));
		System.out.println("Valor n�merico del car�cter " + C + " = " + Character.getNumericValue(C));
		
	}

}
